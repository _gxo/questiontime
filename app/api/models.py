from django.contrib.auth import get_user_model
from django.db import models
from api.helpers import generate_code

User = get_user_model()


class UserProfile(models.Model):
    user = models.OneToOneField(
        verbose_name='user',
        to=User,
        on_delete=models.CASCADE,
        related_name='user_profile'
    )
    city = models.CharField(null=True, blank=True, max_length=30)
    phone = models.CharField(null=True, blank=True, max_length=30)
    bio = models.CharField(null=True, blank=True, max_length=300)
    profile_pic = models.ImageField(null=True, blank=True)
    code = models.CharField(
        verbose_name='code',
        max_length=255,
        default=generate_code,
    )

    def generate_new_code(self):
        self.code = generate_code()
        self.save()
        return self.code

    def __str__(self):
        return self.user.username
